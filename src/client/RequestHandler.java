/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/
Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 *
 * @author Cristian Pamparau
 */
class RequestHandler extends Thread
{
    private final Socket socket;
    RequestHandler( Socket socket )
    {
        this.socket = socket;
    }

    @Override
    public void run()
    {
        try
        {
            System.out.println( "Received a connection" );

            // Get input and output streams
            BufferedReader in = new BufferedReader( new InputStreamReader( socket.getInputStream() ) );
                //PrintWriter out = new PrintWriter( socket.getOutputStream() );

            // Write out our header to the client
            //out.println( "Echo Server 1.0" );
            //out.flush();

            // Echo lines back to the client until the client closes the connection or we receive an empty line
            String line = in.readLine(); 
            System.out.println("Am citit: ");
            System.out.println(line);
            do 
            {
                line=in.readLine();
                System.out.println("Am citit: " + line);
            }while( !"Exit!".equals(line));

            // Close our connection
            in.close();
            //out.close();
            socket.close();

            System.out.println( "Connection closed" );
        }
        catch( IOException e )
        {
            System.out.println(e.getMessage());
        }
    }
}
